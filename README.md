Ceci est le code-source du site web de Codeurs en Liberté (www.codeursenliberté.fr).

Le site est généré par [Hugo](http://gohugo.io/) à partir de fichiers HTML et Markdown.

## Comment développer en local

```
hugo serve
```

## Comment générer le site

```
hugo
```
