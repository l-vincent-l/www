+++
date = "2016-09-04T18:42:31+02:00"
title = "Accueil"
url = "/index.html"
+++

<div class="home__overview">
  Codeurs en Liberté regroupe des salariés indépendants dans le domaine de l’informatique.
</div>

## Compétences

Nos compétences sont le reflet de chaque individu et non pas de l’entreprise.

Actuellement, nous sommes capables de répondre aux questions concernant :

* Protection de la vie privée et contournement de censure ;
* Calcul d’itinéraires (modes routiers et à fiches horaires) ;
* Développement front- et back-end ;
* Cartographie (traitement de données spatiales et rendu).

## Formes d’intervention

Selon les individus et les besoins de nos clients, nous intervenons de diverses
manières :

* Projets de longue durée ;
* Prototypes ;
* Conseil ;
* Formation ;
* Réalisation.

## Travailler avec nous

Vous avez une mission pour nous ? Vous voulez mettre en œuvre une collaboration
informelle et ouverte ? Vous souhaitez devenir membre ?

Envoyez-nous un email à {{< email bonjour >}}
