import http.server, socketserver
import requests, io, os
import logging
from urllib.parse import urlparse, parse_qs
from zipfile import ZipFile
from daemonize import Daemonize

pid = "/tmp/www-deploy.pid"
uid = os.getenv("WWW_DEPLOY_UID")

logger = logging.getLogger(__name__)
fh = logging.FileHandler("www-deploy", "w")
logger.addHandler(fh)
logger.setLevel(logging.INFO)
keep_fds = [fh.stream.fileno()]

PORT    = 8005
SECRET  = os.getenv("WWW_DEPLOY_SECRET")
PATH    = "{CI_PROJECT_URL}/builds/{CI_BUILD_ID}/artifacts/download"
DEST    = os.getenv("WWW_DEPLOY_PATH") + "/{}" 

class Handler(http.server.SimpleHTTPRequestHandler):
    def do_GET(s):
        try:
            params = parse_qs(urlparse(s.path).query)
            logger.info("Requested path: {}".format(s.path))
            if 'secret' in params and params['secret'][0] == SECRET:
                url = PATH.format(CI_PROJECT_URL = params['CI_PROJECT_URL'][0], CI_BUILD_ID = params['CI_BUILD_ID'][0])
                logger.info("Fetching file {}".format(url))
                file = requests.get(url)
                with ZipFile(io.BytesIO(file.content)) as zip:
                    destination = DEST.format(params['CI_PIPELINE_ID'][0])
                    logger.info("Extracting the files into {}".format(destination))
                    zip.extractall(destination)
                logger.info("done !")
                s.send_response(200)
                s.end_headers()
            else:
                logger.warn("403: Access denied")
                s.send_error(403)
                s.end_headers()
        except Exception as e:
            logger.error('Error: {}'.format(e))
            s.send_error(500)
            s.end_headers()

def main():
    httpd = socketserver.TCPServer(("", PORT), Handler)
    logger.info("serving at port", PORT)
    httpd.serve_forever()

daemon = Daemonize(app="www-deploy", pid=pid, action=main, user=uid, keep_fds=keep_fds, logger=logger, verbose=True)
logger.info("Starting the daemon")
daemon.start()
