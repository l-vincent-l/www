build:  ## Generates the static website
	hugo
run:    ## Runs a webserver that updates on changes
	hugo serve
clean:  ## Deletes all the generates files
	rm -r ./public
deploy: build ## Deploy the website in production
	./deploy.sh
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: build run clean deploy help
